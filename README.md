Name: Dhita Putri Pratama
NPM: 1706039465
Class: B
Hobbies : Listening to Music and Reading

##Links
- Git Exercise
https://gitlab.com/dhitapp_/lab-adprog.git
- Javari Park Festival ( in the assignment 3 )
https://gitlab.com/dhitapp_/Tugas-Pemrograman-aka-Assignment/tree/master/assignment-3

##My Notes
Git Branches Usage:
Akan dibuat game dengan fitur tambahan seperti fitur pilih level dalam permainan.
Namun kali ini pembuatan game tidak dilakukan sendiri, melainkan dengan banyak orang
yang kemudian dibutuhkan untuk melakukan Branching.

Hal ini dilakukan dengan cara membuat Branch baru dengan perintah:

git branch <nama_branch>

Lalu untuk setiap level yang yang berbeda, taruh di branch yang berbeda pula.
Kemudian untuk setiap branch, lakukan merge dengan perintah:

git merge <nama_branch>

Secara otomatis, potongan kode dan file dari branch dan master akan disatukan namun dengan conflict.
Tugas selanjutnya adalah dengan memilih section yang akan dipilih. Apakah master atau branch yang lain?
Hapus bagian <<<<< HEAD, =========, dan >>>>> cool-feature.

Lalu lakukan commmit seperti biasa ke branch master.

- Git Revert Usage
Namun saat pembuatan game, saat terjadi conflict, tidak sengaja developer salah memilih conflict yang diinginkan.
Namun hal potongan kode tersebut sudah terlanjut dicommit, sehinga hal yang harus dilakukan ialah dengan melakukan git revert.

Hal yang pertama yang dilakuan adalah dengan memasukkan perintah git log pada git Bash.
Setelah itu masukkan perintah git checkout <commit_hash> untuk masuk ke commit tersebut.
Kemudian lakukan checkout kembali ke branch master. Setelah itu lakukan revert dengan perintah

git revert <commit_hash>

Dengan commit_hash merupakah seri karakter yang merujuk pada suatu commit yang diinginkan.
Akan ada conflict akibat proses revert tersebut. Fiksasi hal tersebut seperti yang telah dilakukan sebelumnya.
Kemudian push seperti biasa. Proses revert akan selesai.